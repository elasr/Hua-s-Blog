// 当前状态
export const stateArr = ["听音乐中...", "敲代码中...", "求职中...", "持续学习中...", "摩拳擦掌中..."];
// 技术栈
export const skillArr = [
    "HTML5 + CSS3 + ES6",
    "jQuery",
    "Bootstrap",
    "Ajax",
    "Axios",
    "Git",
    "Webpack + Vite",
    "Less + Scss",
    "Vue2 + VueRouter + Vuex",
    "Vue3 + VueRouter + Pinia",
    "TypeScript",
    "Node + Koa + Express",
    "Vant",
    "ElementUi + ElementPlus",
    "微信小程序",
    "UniApp",
    "Nuxt",
    "Tailwindcss",
    "...",
];

// 项目经验

export const weTalk = {
    name: "WeTalk",
    id: "weTalk",
    isRebuild: false,
    desc: "该项目是一款<b>多人即时在线聊天平</b>台 。用户可以搜索添加陌生人和群聊、进行单聊和群聊、发送语音文字图片、进行<b>语音视频聊天</b>。创建群聊、邀请好友加入群聊、 发送群公告、 设置管理员。修改个人信息, 发布朋友圈、查看好友朋友圈、进行点赞评论等操作。数据库使用mysql，配合H5的本地存储功能，实现<b>离线消息缓存</b>。",
    skill: ["Vue2", "VueRouter", "Axios", "Vuex", "Socket.io", "Peer", "Node", "Express", "MySQL", "Vant2", "Scss"],
    responsibility: [
        "基于Axios 的请求拦截器实现验证登录功能, 使用Socket.io实现邮箱实时注册。",
        "使用基于 WebSocket 封装的 Socket.io 和 Vuex 插件,实现消息、用户、通知管理。",
        "使用 localStorage 实现消息消息已读未读的状态管理。",
        "使用 H5 的获取用户媒体API实现语音消息录制。",
        "使用基于WebRtc封装的 Peer.js 三方库完成实时音频流、视频流通讯。",
        "使用 MySql2 操作数据库，完成对数据库增删改查的需求。",
        "使用 Multer 让 Express 框架支持音频、单图片、多图片上传。",
        "二次封装 Socket.io 库，简化 Socket 通讯流程，让代码更加模块化，与普通http请求联通。",
        "使用 NodeMailer 实现邮件验证。",
        "二次封装 JsonWebToken 库，实现登录鉴权。",
    ],
};

export const weTalkVue3 = {
    name: "WeTalk —— Vue3 重构",
    id: "weTalkVue3",
    isRebuild: true,
    desc: "使用Vue3对Wetalk进行重构, 新增了全局主题自定义功能、<b>语音可视化</b>、<b>地理位置查询</b>。",
    skill: ["Vue3", "Typescript", "Vite", "Pinia", "WaveSurfer", "Vant3"],
    responsibility: [
        "使用 Vue3+Typescript+Vite+Pinia 完成全部页面的重构。",
        "使用 pinia-plugin-persist 实现重要数据本地存储，离线下也能浏览部分页面。",
        "使用百度地图API，用户可以发送地理位置和定位，并查看别人的地理位置消息。",
        "使用 WaveSurfer.js 实现语音消息音频可视化，用户观感更好。",
        "新增主题自定义模块，使用FileReader 将主题图片Base64转码存储到本地。",
        "重写通话逻辑，更加合理，将部分代码分离成单独文件，方便后期维护。",
    ],
};

export const weibo = {
    name: "微博 —— Web移动端",
    id: "weibo",
    isRebuild: false,
    desc: "使用微博真实接口对微博Web移动端进行复刻，完成了80%的功能。未登录状态下，可以浏览首页11个分栏内的不同数据，也可以搜索指定的内容，根据内容不同也 有13个不同分栏，超话、用户页面也有不同的数量分栏内容... 登录状态下：可以点赞、评论、转发微博，发送、删除微博，关注其它用户，分组、聊天...",
    skill: ["Vue2", "Vuex", "Vant2", "Axios", "Day", "Lodash", "VueCookie", "Scss"],
    responsibility: [
        "二次封装 Axios 拦截器，配合路由守卫，未登录状态下无法使用部分功能。",
        "使用 Vant 的骨架屏、Toast在请求数据的时候给予相应提示，保证良好的用户体验。",
        "用户下拉刷新获取新数据，浏览到指定位置自动获取更多数据。",
        "将微博十多种数据对应的组件汇总到一个组件内，也可单独分离，代码可维护性强。",
        "使用 keep-alive 组件缓存数据，配合生命周期来进行处理，保证良好的用户体验。",
        "使用 Day.js 格式化微博发送时间、聊天消息时间，使用长轮询更新聊天消息。",
        "使用 Lodash.js 的节流避免多次点击请求数据造成服务器卡顿。",
        "使用 Vuex 对公共数据进行管理，避免数据被更新时其它组件获取到旧数据。",
        "使用 Scss 预编译语言 提升开发效率，避免原生 CSS 带来的重复的代码，方便维护。",
    ],
};

export const project = [weTalk, weTalkVue3, weibo];
