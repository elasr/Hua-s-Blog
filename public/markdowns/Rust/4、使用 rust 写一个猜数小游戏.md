## 猜数游戏

```rust
// 导入标准库
use std::io;
// 导入随机数模块
use rand::Rng;
// cmp 是 compare 的缩写，这个Order是一个枚举，是比较的三个结果。
use std::cmp::Ordering;
fn main() {
    println!("欢迎来到猜数小游戏！！！");
    println!("请输入一个数字：");

    // 生成一个1~100的随机数，和js的随机数一样取前不取后。
    let secret_num = rand::thread_rng().gen_range(1..101);

    

    // 声明类似while的循环，除非调用break关键字，否则无法跳出。
    loop {
        // let 声明变量
        // mut 标识变量是可变的，在rust中变量默认是immutable（不可修改）的。
        // String::new() 是生成一个字符串对象。
        // 注意！！！！
        // 必须要把该声明写入 loop 里面！因为下方io输入是类似添加的操作而不是
        // 覆盖！，所以所有的输入都会被加到一起！包括回车！
        let mut guess = String::new();

        // 预先导入标准库。
        // io 用于处理用户输入输出。
        // stdin 是 io库的一个方法，用于接收输入
        // read_line() 接收输入，将数据付给另一个变量，
        // 参数是一个可变的变量引用（& 符号就是引用的意思）。
        // expect() 用于处理异常，返回值result是一个枚举类型 ，如果值是Ok，
        // 表示无异常，如果值是 Err 则会退出程序。
        io::stdin().read_line(&mut guess).expect("无法接收输入！");
        // 使用的时候导入标准库
        // std::io::stdin().read_line(&mut guess).expect("无法接收输入！");

        // 处理异常，如果转换数字失败则报错并让用户继续猜。
        // 因为数字与字符串无法比较，所以需要转换，需要使用显式声明，这样才能转换为对应的数据。
        // u32 是一种无符号的整数类型。
        let guess:u32 = match guess.trim().parse() {
            // 结果result是一个枚举类型，所以可以使用match关键字。
            // 根据枚举的值来对应不同的处理逻辑。 
            Ok(num) => num,
            // 使用_来忽略字段
            Err(_) => {
                println!("你输入的是：{}", guess);
                println!("请输入1~100的整数！");
                continue
            },
        };

        // {} 是一个变量占位符，如果有两个变量，则{}{}，以此类推...
        println!("你输入的数字是：{}", guess);

        match guess.cmp(&secret_num) {
            Ordering::Equal => {
                println!("猜对了！");
                break; // 跳出循环。
            },
            Ordering::Greater => println!("猜大了！"),
            Ordering::Less => println!("猜小了！")
        }
    }

    


}

```
