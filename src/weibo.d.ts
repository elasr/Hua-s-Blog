interface WeiboType {
    [index: string]: string;
}

interface ProjectProfileType {
    name: string;
    id: string;
    isRebuild: boolean;
    desc: string;
    skill: string[];
    responsibility: string[];
}
