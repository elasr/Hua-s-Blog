interface PlayerControllerType {
    playerState: boolean;
    src: string;
    toggle: () => void;
}
