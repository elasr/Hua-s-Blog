// store/filters.ts
import { defineStore } from "pinia";
import { getLike } from "~~/assets/apis";

let src = getLike();

const usePlayerControllerStore = defineStore("PlayerController", {
    state: () => {
        return {
            playerState: false,
            src,
            timeID: <any>null,
            hide: true,
        };
    },
    actions: {
        toggle() {
            this.playerState = !this.playerState;
            this.hide = false;
            clearInterval(this.timeID);
            this.timeID = setInterval(() => {
                this.hide = true;
            }, 3000);
        },
        hover() {
            clearInterval(this.timeID);
            this.hide = false;
        },
        blur() {
            clearInterval(this.timeID);
            this.hide = true;
            this.timeID = setInterval(() => {
                this.hide = true;
            }, 3000);
        },
    },
});

export default usePlayerControllerStore;
