import * as path from "node:path";
import * as fs from "node:fs";

/**
 * 读取public目录下的静态资源。
 * @param requestPath 该文件在public目录下的文件路径，需要包含文件名、文件扩展名，比如 /markdowns/test.md
 * @param encoding 编码格式，默认为utf-8.
 * @returns 读取的markdown文件内容。
 */
const getLocalResource = async (
  requestPath: string,
  encoding: BufferEncoding = "utf-8"
): Promise<string> => {
  const filePath = path.join(process.cwd(), "public", requestPath);
  console.log("filePath => ", filePath);
  const data = await fs.promises.readFile(filePath, encoding);
  return data;
};

export default defineNuxtPlugin((nuxtApp) => {
  return {
    provide: {
      localResource: {
        getLocalResource,
        /**
         * 读取public目录下的markdown文件。
         * @param requestPath 该文件在public目录下的文件路径，需要包含文件名、文件扩展名，比如 /test.md
         * @param encoding 编码格式，默认为utf-8.
         * @returns 读取的markdown文件内容。
         */
        getMarkDownAsString: (filePath: string): Promise<string> =>
          getLocalResource("/markdowns" + filePath, "utf-8"),
        /**
         * 获取markdowns文件夹下的所有目录，可用作文档类目。
         * @returns String[] 举例：[ 'C++', 'React', 'Rust', 'Vue' ]
         */
        getMarkdownDirs: (): string[] => {
          return fs.readdirSync(
            path.join(process.cwd(), "public", "markdowns")
          );
        },
      },
    },
  };
});
