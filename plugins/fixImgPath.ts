import { baseURL } from "~~/assets/apis";

export default defineNuxtPlugin((nuxtApp) => {
    return {
        provide: {
            fixImgPath: (str: string) => baseURL + str,
        },
    };
});
