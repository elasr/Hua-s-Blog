Ts-node允许我们指向一个Typescript文件。它将运行.ts ，为我们编译并在Node.js中运行它。
```bash
npm install -D ts-node
```

>当使用Ts-node时，确保你的本地项目中安装了Typescript。要安装它，请运行npm install -D typescript 。

另外，你还需要在 tsconfig.json 中添加 `"ts-node": { "esm": true }`
```json
{
  // https://nuxt.com/docs/guide/concepts/typescript
  "extends": "./.nuxt/tsconfig.json",
  "ts-node": { "esm": true }
}

```

然后就可以使用以下命令运行 `.ts` 文件了：
```bash
ts-node my-file.ts
```