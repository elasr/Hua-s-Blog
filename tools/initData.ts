import * as path from "node:path";
import * as fs from "node:fs";

const initNotesList = () => {
  const noteBasePath = path.resolve(process.cwd(), "../public/markdowns");

  console.log("noteBasePath ", noteBasePath);

  const dirs = fs
    .readdirSync(noteBasePath, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);

  const notesArr = dirs
    .map((dName) => ({
      name: dName,
      path: path.resolve(noteBasePath, dName),
    }))
    .map((noteDir) => {
      return {
        name: noteDir.name,
        path: noteDir.path,
        files: fs
          .readdirSync(noteDir.path, { withFileTypes: true })
          .filter((dirent) => dirent.isFile())
          .map((dirent) => dirent.name),
      };
    });

  const data = JSON.stringify(notesArr);

  fs.writeFileSync(
    path.resolve(process.cwd(), "../public/markdowns/notes.json"),
    data
  );
  // console.log("data => ", data);
};

initNotesList();
