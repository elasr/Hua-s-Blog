/** @type {import('tailwindcss').Config} */
module.exports = {
    purge: ["./components/**/*.{vue,js}", "./layouts/**/*.vue", "./pages/**/*.vue", "./plugins/**/*.{js,ts}", "./nuxt.config.{js,ts}"],
    darkMode: "class", // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                themeBackground: "var(--background)",
                themeText: "var(--text)",
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
};
